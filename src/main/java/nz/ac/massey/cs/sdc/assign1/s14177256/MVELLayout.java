package nz.ac.massey.cs.sdc.assign1.s14177256;

import java.util.HashMap;
import java.util.logging.Logger;

import org.apache.log4j.Layout;
import org.apache.log4j.spi.LoggingEvent;
import org.mvel2.*;
import org.mvel2.templates.TemplateRuntime;
import org.mvel2.util.Make.List;
import org.mvel2.util.Make.Map;

public class MVELLayout extends Layout{
	private static final String newLine = System.getProperty("line.separator").toString();
//	public static void setLayout(LoggingEvent e) {
		
//		String expression =  "['Bob' : 'b', 'Michael' :'m']" ;
//        Map o = (Map ) MVEL.eval(expression);
//        System.out.println((String)o.get("Bob"));
//		System.out.println("Hello");
//		HashMap<String, String> params = new HashMap<String, String>();
//		params.put("a", "@");
//		params.put("data", "D");
//		String result = (String) MVEL.eval("a", params);
//		System.out.println(result);
		//"%p [@ %d{dd MMM yyyy HH:mm:ss} in %t] %m%n";
//		String template = "[@{t}] @{p}: @{m} @{n} Catagory: @{c} : @{d}, @{n} ===========================";
//		HashMap vars = new HashMap();
//		
//		vars.put("c",e.categoryName);
//		vars.put("d",e.toString() );
//		vars.put("m",e.getMessage() );
//		vars.put("p",e.getLevel() );
//		vars.put("t",e.getThreadName() );
//		vars.put("n",newLine);
//		System.out.println(TemplateRuntime.eval(template, vars));
        
//	}
//	@Override
//	public void activateOptions() {
//		// TODO Auto-generated method stub
//		
//	}
	@Override
	public String format(LoggingEvent e) {
		// TODO Auto-generated method stub
		String template = "[@{t}]@{p}:@{m} Catagory:@{c}";
		HashMap vars = new HashMap();
		
		vars.put("c",e.categoryName);
		vars.put("d",e.toString() );
		vars.put("m",e.getMessage() );
		vars.put("p",e.getLevel() );
		vars.put("t",e.getThreadName() );
		vars.put("n",newLine);
		return (String) TemplateRuntime.eval(template, vars);
	}
	@Override
	public boolean ignoresThrowable() {
		// TODO Auto-generated method stub
		return false;
	}


}

