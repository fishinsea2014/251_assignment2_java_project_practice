package nz.ac.massey.cs.sdc.assign1.s14177256;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

import org.mvel2.MVEL;
import org.mvel2.templates.CompiledTemplate;
import org.mvel2.templates.SimpleTemplateRegistry;
import org.mvel2.templates.TemplateCompiler;
import org.mvel2.templates.TemplateRegistry;
import org.mvel2.templates.TemplateRuntime;

public class MVELTemplateWriter {
	
	

	private final CompiledTemplate template;

    /**
     * Constructor for MVELTemplateWriter.
     *
     * @param template the MVEL template
     */
    public MVELTemplateWriter(String template) {
        super();
        this.template = TemplateCompiler.compileTemplate(template);
    }

    /**
     * Merge an Object with the template and write the output
     * to f.
     *
     * @param o the Object
     * @param f the output File
     */
    public void write(Object o){
        String output = (String) TemplateRuntime.execute(template, o);
        System.out.println(output);
    }
}
