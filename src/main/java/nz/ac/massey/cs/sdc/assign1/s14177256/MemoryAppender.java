package nz.ac.massey.cs.sdc.assign1.s14177256;

import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.AppenderSkeleton;
import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.spi.LoggingEvent;

public class MemoryAppender extends AppenderSkeleton {
	private static int maxSize=5000;
	private static List<SoftReference<LoggingEvent>> events;
	private static MemoryAppender lastInstance;
	//private static int messageCount=0;
	private static List<SoftReference<LoggingEvent>> secondaryLogList=new ArrayList<SoftReference<LoggingEvent>>();
	
	MemoryAppender(){
		super();
		lastInstance = this;
		events = new ArrayList<SoftReference<LoggingEvent>>();
	}
	
	public static MemoryAppender getInstance(){
		if (lastInstance == null){
			lastInstance = new MemoryAppender();
		}
		return lastInstance;
	}
	
	public static List<SoftReference<LoggingEvent>> getLogs(){
		//System.out.println(events.size());
		//for (LoggingEvent l:events)
		//	secondaryLogList.add(new SoftReference<>(l));
		
		List<SoftReference<LoggingEvent>> mergedList = new ArrayList<SoftReference<LoggingEvent>>();
		for (int i=0; i<events.size();i++) 
			mergedList.add(events.get(i));
		mergedList.addAll(secondaryLogList);
		return  Collections.unmodifiableList(mergedList);
	}
	
	public static void initialize(){
		events.clear();
		secondaryLogList.clear();
		lastInstance = null;
		
		//messageCount=0;
		
	}

	public void close() {
		// TODO Auto-generated method stub
		this.closed= true;
	}

	public boolean requiresLayout() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	protected void append(LoggingEvent event) {
		// TODO Auto-generated method stub
		if (events.size()<maxSize){
			//events.add(new SoftReference<>(event));
			events.add(new SoftReference<>(event));
			//messageCount +=1;
			return;
		}
		else{
			//for (LoggingEvent l:events) secondaryLogList.add(new SoftReference<>(l));
//			for (int i=0; i<maxSize;i++) 
//				secondaryLogList.add(new SoftReference<>(events.get(i)));
			secondaryLogList.addAll(events);
			System.out.println("secnod list is size of:"+secondaryLogList.size());
			
			events.clear();
			//lastInstance=null;
			events.add(new SoftReference<>(event));
		}
		
		//System.out.println(messageCount);
		
	}
}