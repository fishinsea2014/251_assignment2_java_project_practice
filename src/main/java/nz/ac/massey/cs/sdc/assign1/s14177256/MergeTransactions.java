package nz.ac.massey.cs.sdc.assign1.s14177256;

import java.io.BufferedReader;
import java.io.Console;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import org.apache.log4j.spi.LoggingEvent;
//import org.apache.velocity.runtime.parser.node.GetExecutor;



/**
 * The purpose of this class is to read and merge financial transactions, and print a summary:
 * - total amount 
 * - highest/lowest amount
 * - number of transactions 
 * @author jens dietrich
 */
public class MergeTransactions {

	private static DateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yyyy");
	private static NumberFormat CURRENCY_FORMAT = NumberFormat.getCurrencyInstance(Locale.getDefault());
	
	//private static final Logger a1 = Logger.getLogger("FILE");
	private static final Logger transactionLogger = Logger.getLogger("TRANSACTIONS");
	private static final Logger a1=Logger.getLogger("memoryLogger");
	
	
	public static void main(String[] args) throws Exception {
		
		
		
		BasicConfigurator.configure();
		
//		a1.addAppender(
//			new org.apache.log4j.FileAppender(
//				new org.apache.log4j.SimpleLayout(), 
//				"logs.txt"
//			)
//		);
		
		// advanced part - create csv
//		a1.addAppender(
//				new org.apache.log4j.FileAppender(
//					new org.apache.log4j.PatternLayout("%p,%d{dd MMM yyyy HH:mm:ss},%t,%m%n"), 
//					"logs.csv"
//				)
//			);
		transactionLogger.addAppender(
				new org.apache.log4j.FileAppender(
					new MVELLayout(), 
					"logs.txt"
				)
			);
		
		// example - only log at INFO level - will switch off debug statements ! 
		a1.setLevel(Level.INFO);
		//a1.setLevel(Level.INFO);
		a1.addAppender(MemoryAppender.getInstance());
		a1.info("1 Hello");
		
		
				
		
//		log4j.rootLogger = TRACE, A1;
		
		List<Purchase> transactions = new ArrayList<Purchase>();
		
		// read data from 4 files
		readData("transactions1.csv",transactions);
		readData("transactions2.csv",transactions);
		readData("transactions3.csv",transactions);
		readData("transactions4.csv",transactions);
		
		// print some info for the user
		transactionLogger.info("" + transactions.size() + " transactions imported");
		transactionLogger.info("total value: " + CURRENCY_FORMAT.format(computeTotalValue(transactions)));
		transactionLogger.info("max value: " + CURRENCY_FORMAT.format(computeMaxValue(transactions)));
		
		
		System.out.println("==========Memory Appender=====================");
		System.out.println(MemoryAppender.getLogs().size());
		
		for (int i=0;i<MemoryAppender.getLogs().size();i++) {
			System.out.println("Memory Appender result:"+i+", "+MemoryAppender.getLogs().get(i).get().getMessage().toString());
			//System.out.println("Memory Appender result:"+i+", "+MemoryAppender.getLogs().get(i).getMessage().toString());
		}
		
		System.out.println("==========MVEL format=====================");
		MVELLayout mvelLayout= new MVELLayout();
		for (int i=0;i<MemoryAppender.getLogs().size();i++) {
			System.out.println(mvelLayout.format((MemoryAppender.getLogs().get(i).get())));
			//MVELLayout.setLayout(MemoryAppender.getLogs().get(i));
			
		}
		
		System.out.println("=========================================");

	}
	
	private static double computeTotalValue(List<Purchase> transactions) {
		double v = 0.0;
		for (Purchase p:transactions) {
			v = v + p.getAmount();
		}
		return v;
	}
	
	private static double computeMaxValue(List<Purchase> transactions) {
		double v = 0.0;
		for (Purchase p:transactions) {
			v = Math.max(v,p.getAmount());
		}
		return v;
	}

	// read transactions from a file, and add them to a list
	private static void readData(String fileName, List<Purchase> transactions) {
		
		File file = new File(fileName);
		String line = null;
		// print info for user
		a1.info("2 import data from " + fileName);
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(file));
			while ((line = reader.readLine())!=null) {
				String[] values = line.split(",");
				Purchase purchase = new Purchase(
						values[0],
						Double.parseDouble(values[1]),
						DATE_FORMAT.parse(values[2])
				);
				transactions.add(purchase);
				// this is for debugging only
				a1.debug("3 imported transaction " + purchase);
			} 
		}
		catch (FileNotFoundException x) {
			// print warning
			a1.error("4 file " + fileName + " does not exist - skip",x);
		}
		catch (IOException x) {
			// print error message and details
			a1.error("5 problem reading file " + fileName,x);
		}
		// happens if date parsing fails
		catch (ParseException x) { 
			// print error message and details
			a1.error("6 cannot parse date from string - please check whether syntax is correct: " + line,x);	
		}
		// happens if double parsing fails
		catch (NumberFormatException x) {
			// print error message and details
			a1.error("7 cannot parse double from string - please check whether syntax is correct: " + line,x);	
		}
		catch (Exception x) {
			// any other exception 
			// print error message and details
			a1.error("8 cannot parse double from string - please check whether syntax is correct: " + line,x);	
		}
		finally {
			try {
				if (reader!=null) {
					reader.close();
				}
			} catch (IOException e) {
				// print error message and details
				a1.error("9 cannot close reader used to access " + fileName,e);
			}
		}
	}

}