package nz.ac.massey.cs.sdc.assign1.s14177256;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class Test {
	private static Logger loggerMemory;
	private static Logger loggerConsole;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		loggerMemory = Logger.getLogger("Memory");
    	loggerConsole = Logger.getLogger("Console");
    	loggerMemory.addAppender(MemoryAppender.getInstance());
    	loggerMemory.setLevel(Level.INFO);
		for (int i=0;i<10;i++){
			loggerMemory.info("message"+i);
		}
		
		for (int i=0;i<MemoryAppender.getLogs().size();i++) {
			System.out.println("Memory Appender result:"+i+", "+MemoryAppender.getLogs().get(i).get().getMessage().toString());
			//System.out.println("Memory Appender result:"+i+", "+MemoryAppender.getLogs().get(i).getMessage().toString());
		}

	}

}
