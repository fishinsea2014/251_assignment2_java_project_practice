package test.nz.ac.massey.cs.sdc.assign1.s14177256;

import static org.junit.Assert.*;

import java.io.Console;

import javax.naming.InitialContext;
import javax.sound.midi.MidiDevice.Info;

import org.apache.log4j.Appender;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.*;

import nz.ac.massey.cs.sdc.assign1.s14177256.MVELLayout;
import nz.ac.massey.cs.sdc.assign1.s14177256.MemoryAppender;

/**
 * Unit test for MemoryAppender and MVELLayout
 */
public class MemoryAppenderAndMVELLayoutTest    
{
    /**
     * Create the test case
     *
     */
	private static Logger loggerMemory;
    public MemoryAppenderAndMVELLayoutTest()
    {
        super();
    }
    
    @BeforeClass
    public static void initial(){
    	loggerMemory = Logger.getLogger("Memory");
    	loggerMemory.addAppender(MemoryAppender.getInstance());
    }
    
    @Before 
	public void setup() {
    	//Logger loggerMemory;
    	MemoryAppender.initialize();
    	
	}
	@After 
	public void tearDown() {
//		loggerMemory=null;
//		loggerConsole=null;
		//MemoryAppender.initialize();
		//MemoryAppender.close();
		//loggerMemory.shutdown();;
	}

	@Test
    public void testMemoryAppenderWithTenInfoBySize()
    {
		//MemoryAppender.initialize();
		loggerMemory.setLevel(Level.INFO);
		for (int i=0;i<10;i++){
			loggerMemory.info("message"+i);
		}
		assertEquals(10, MemoryAppender.getLogs().size());
    }
	
	@Test
    public void testMemoryAppenderWithOneInfoByString()
    {
		
		//MemoryAppender.initialize();
		loggerMemory.setLevel(Level.INFO);
		loggerMemory.info("message"+1);
		
		assertEquals("message1", MemoryAppender.getLogs().get(0).get().getMessage().toString());
    }
	
	@Test
    public void testMemoryAppenderWithTwentyInfoBySize()
    {
		//MemoryAppender.initialize();
		loggerMemory.setLevel(Level.INFO);
		for (int i=0;i<20;i++){
			loggerMemory.info("message"+i);
		}
		assertEquals(20, MemoryAppender.getLogs().size());
    }
	
	@Test
    public void testMemoryAppenderByLevelINFO()
    {
		//MemoryAppender.initialize();
		loggerMemory.setLevel(Level.INFO);
		loggerMemory.warn("warn1");
		loggerMemory.debug("debug1");
		loggerMemory.info("info1");
		assertEquals(2, MemoryAppender.getLogs().size());
    }
	
	@Test
    public void testMemoryAppenderByLevelTRACE()
    {
		//MemoryAppender.initialize();
		loggerMemory.setLevel(Level.TRACE);
		loggerMemory.warn("warn 1");
		loggerMemory.debug("debug 1");
		loggerMemory.trace("trace 1");
		assertEquals(3, MemoryAppender.getLogs().size());
    }
	
	@Test
	public void testMVELLayoutWithMemoryAppender(){
		
		MVELLayout mvelLayout=new MVELLayout();
		loggerMemory.warn("It is a warn.");
		String expected = "[main]WARN:It is a warn. Catagory:Memory";

		assertEquals(expected, mvelLayout.format(MemoryAppender.getLogs().get(0).get()));
	}
}
