package test.nz.ac.massey.cs.sdc.assign1.s14177256.StressTest;

import static org.junit.Assert.*;

import java.io.Console;

import javax.naming.InitialContext;
import javax.sound.midi.MidiDevice.Info;

import org.apache.log4j.Appender;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.spi.LoggerFactory;
import org.junit.*;

import nz.ac.massey.cs.sdc.assign1.s14177256.MVELLayout;
import nz.ac.massey.cs.sdc.assign1.s14177256.MemoryAppender;
/**
 * Stress test for MemoryAppender and MVELLayout.
 */
public class StressTEst    
{
	private static Logger loggerMemory,loggerConsole;
	//private static final Logger LOGGER = LoggerFactory.getLogger(PlaceCommitter.class);
	
    public StressTEst()
    {
        super();
    }
    
    @BeforeClass
    public static void initial(){
    	loggerMemory = Logger.getLogger("Memory");
    	loggerMemory.addAppender(MemoryAppender.getInstance());
    	
    }
    
    @Before 
	public void setup() {
    	//Logger loggerMemory;
    	MemoryAppender.initialize();
    	
	}
	@After 
	public void tearDown() {
//		loggerMemory=null;
//		loggerConsole=null;
		//MemoryAppender.initialize();
		//MemoryAppender.close();
		//loggerMemory.shutdown();;
	}
	
	@Test
	public void StressTestForMemoryAppender(){
		loggerMemory.setLevel(Level.INFO);
		for (int i=0;i<10000000;i++){
			loggerMemory.info("message"+i);
			loggerMemory.error("error"+i);
		}
		assert(true);
		
	}

    
}
