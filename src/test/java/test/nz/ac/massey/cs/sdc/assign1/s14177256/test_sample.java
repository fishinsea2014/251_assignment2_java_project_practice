package test.nz.ac.massey.cs.sdc.assign1.s14177256;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class test_sample 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public test_sample( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( test_sample.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp()
    {
        assertTrue( true );
    }
}
